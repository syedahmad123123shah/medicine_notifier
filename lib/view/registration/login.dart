import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.all(12.sp),
            child: TextField(
              decoration: InputDecoration(
                  suffixIcon: Icon(Icons.person),
                  labelText: 'Username',
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.blue),
                      borderRadius: BorderRadius.circular(20.sp))),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(12.sp),
            child: TextField(
              decoration: InputDecoration(
                  suffixIcon: Icon(Icons.remove_red_eye),
                  labelText: 'Password',
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.blue),
                      borderRadius: BorderRadius.circular(20.sp))),
            ),
          )
        ],
      ),
    );
  }
}
