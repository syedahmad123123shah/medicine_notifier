import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:medicine_notifier/view/registration/login.dart';
import 'package:medicine_notifier/view/registration/signup.dart';
import 'package:sizer/sizer.dart';

class ChooseForRegistration extends StatelessWidget {
  const ChooseForRegistration({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        decoration: BoxDecoration(
            color: Colors.transparent,
            image: DecorationImage(
                image: AssetImage('assets/manik-roy-u7GtZ0yVijw-unsplash.jpg'),
                fit: BoxFit.cover)),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InkWell(
                onTap: () {
                  print('login Pressed');
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (ctx) => Login(),
                    ),
                  );
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: Colors.teal,
                      gradient: LinearGradient(
                          colors: [Colors.blue.shade100, Colors.blue]),
                      borderRadius:
                          BorderRadius.only(topRight: Radius.circular(5.sp))),
                  height: 7.h,
                  width: 50.w,
                  child: Text(
                    'Login',
                    style: GoogleFonts.roboto(
                        color: Colors.white,
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              ),
              SizedBox(
                height: 3.h,
              ),
              InkWell(
                onTap: () {
                  print('Signup Pressed');
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (ctx) => Signup(),
                    ),
                  );
                },
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: Colors.teal,
                      gradient: LinearGradient(
                          colors: [Colors.teal.shade100, Colors.teal]),
                      borderRadius:
                          BorderRadius.only(topRight: Radius.circular(5.sp))),
                  height: 7.h,
                  width: 55.w,
                  child: Text(
                    'Signup',
                    style: GoogleFonts.roboto(
                        color: Colors.white,
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
